FROM library/ubuntu:17.10
MAINTAINER Florian Thöni <florian.thoni@floth.fr>

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update; \
   apt-get install -y build-essential curl python-pip && \
   apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.6/install.sh | bash
RUN bash -c ". ~/.nvm/nvm.sh && nvm install 6"